FROM socialengine/rancher-cron:latest
ENV USER_TIMEZONE="America/Edmonton"
RUN apk add tzdata && \
    cp /usr/share/zoneinfo/$USER_TIMEZONE /etc/localtime && \
    echo $USER_TIMEZONE > /etc/timezone 
